using Toybox.Graphics as Gfx;
using Toybox.WatchUi as Ui;
using Toybox.Application as App;

(:glance)
class GlanceView extends Ui.GlanceView {
  var name;
  var yCenter;

  function initialize() {
    GlanceView.initialize();
  }

  function onLayout(dc) {
    name = "TIMER WIDGET";
    yCenter = dc.getHeight() /2;
  }

  function onUpdate(dc) {
    dc.setColor(Gfx.COLOR_WHITE, Gfx.COLOR_BLACK);
    dc.drawText(0, yCenter, Gfx.FONT_GLANCE, name, Gfx.TEXT_JUSTIFY_LEFT | Gfx.TEXT_JUSTIFY_VCENTER );
  }
}
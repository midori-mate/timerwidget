//!
//! Copyright 2020 by HarryOnline
//!
//! Progressbar on tall watches
//!

using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;

class ProgressTall extends Ui.Drawable {

    function initialize(params) {
        Drawable.initialize(params);
    }

	function draw(dc) {
		var fraction = timer.fraction();
		var length = 1 + fraction * dc.getHeight();
        var w = 18;
        var x = dc.getWidth() - w/2;
        var y = dc.getHeight();
    	dc.setPenWidth(w);
        dc.setColor(Gfx.COLOR_RED, Gfx.COLOR_TRANSPARENT);
		dc.drawLine(x, y, x, y-length);   
	} 
}